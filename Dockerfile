FROM debian:bookworm

# Install 
RUN apt-get update
RUN apt-get install git -y

# Execute
CMD ["git", "--version"]
CMD ["echo", "hallo"]
CMD ["cd", "/var/"]
CMD ["ls", "-lia"]